const jwt = require("jsonwebtoken");
const moment = require('moment-timezone');
const User = require("../Model/schema.user");
const { validateRequest } = require('./helper/error_handler');
const cloudinary = require("../Services/Cloudinary");
const mailer = require("../Services/Email/NodeMailer");
const bcrypt = require("bcrypt");
const { checkRequestBodyAddress } = require("./helper/addres_validation");

exports.authSignIn = async (req, res, next) => {
    try {
        validateRequest(req)
        let findUser = await User.findOne({ username: req.body.username });
        if(findUser === null){
            let error = new Error('Username is not found');
            error.statusCode = 501;
            throw error;
        }
        const validate = await bcrypt.compare(req.body.password, findUser.password);  
        if(!validate){
            let error = new Error('Password does not match.');
            error.statusCode = 501;
            throw error;
        }
        let updateUser = await User.findOneAndUpdate(
            { username: req.body.username },
            {
                $set: {
                    "updatedAt": moment().utc("Asia/Singapore").format()
                }
            },
            {
                new: true,
            }
        );
        const token = await jwt.sign({
            id: updateUser._id,
            first_name: updateUser.first_name,
            last_name: updateUser.last_name,
            username: updateUser.username,
            user_picture: updateUser.user_picture,
            address: updateUser.address,
            email: updateUser.email,
            phone_number: updateUser.phone_number,
            role: updateUser.role
        }, process.env.JWT_BACKEND, { 
            expiresIn: '24h'
        });
        
        res.status(200).send({
            status: {
                id: updateUser._id,
                first_name: updateUser.first_name,
                last_name: updateUser.last_name,
                username: updateUser.username,
                user_picture: updateUser.user_picture,
                address: updateUser.address,
                email: updateUser.email,
                phone_number: updateUser.phone_number,
                role: updateUser.role
            },
            payload: token
        })
    } catch(err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
};

exports.authSignUp = async (req, res, next) => {
    try {
        if(!req.file){
            throw err = new Error("Missing image in the request body.")
        }

        let address = JSON.parse(req.body.address);

        const uploadImageResult = await cloudinary.uploader.unsigned_upload(req.file.path, 'cloud-shipping');
        if(!uploadImageResult){
            throw err = new Error("Upload failed")
        }

        checkRequestBodyAddress(address.region, address.province, address.municipality, address.barangay, next)
    
        let newUser;
        let result;
        
        if(req.body.role === "Client") {
            newUser = await User.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                username: req.body.username,
                user_picture: uploadImageResult.secure_url,
                cloudinary_public_ip: uploadImageResult.public_id,
                address: address,
                email: req.body.email,
                phone_number: req.body.phone_number,
                role: req.body.role,
                password: req.body.password
            })
            result = await mailer.sendRegistrationDataToClient({
                firstname: newUser.first_name,
                last_name: newUser.last_name,
                username: newUser.first_name,
                email: newUser.email,
                phone_number: newUser.phone_number,
                password: req.body.password
            });
        } else if(req.body.role === "Delivery Partner") {
            newUser = await User.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                username: req.body.username,
                user_picture: uploadImageResult.secure_url,
                cloudinary_public_ip: uploadImageResult.public_id,
                address: address,
                email: req.body.email,
                phone_number: req.body.phone_number,
                role: req.body.role,
                // vehicles: req.body.vehicles,
                // licensed_no: req.body.licensed_no,
                // Vehicle & License will be added in the dashboard
                password: req.body.password
            });
            result = await mailer.sendRegistrationDataToDeliveryPartner({
                firstname: newUser.first_name,
                last_name: newUser.last_name,
                username: newUser.first_name,
                email: newUser.email,
                phone_number: newUser.phone_number,
                password: req.body.password
            });
        }
        
        const token = await jwt.sign({
            id: newUser._id,
            first_name: newUser.first_name,
            last_name: newUser.last_name,
            username: newUser.username,
            organization_name: newUser.organization_name,
            address: newUser.address,
            email: newUser.email,
            phone_number: newUser.phone_number,
            role: newUser.role,
            industry: newUser.industry,
            vehicles: newUser.vehicles,
            licensed_no: newUser.licensed_no
        }, process.env.JWT_BACKEND, { 
            expiresIn: '24h'
        });

        res.status(200).send({
            message: "Account has been created successfully!",
            status: newUser,
            email_status: result.accepted ? result.accepted.length >= 1 ? true : false : false,
            payload: token
        });

    } catch(err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
};

exports.integrationSignUp = async (req, res, next) => {
    try {
        validateRequest(req)
        const uploadImageResult = await cloudinary.uploader.unsigned_upload(req.file.path, 'cloud-shipping');
        let newAPIUser = await User.create({
            username: req.body.username,
            organization_name: req.body.organization_name,
            organization_picture: uploadImageResult.secure_url,
            cloudinary_public_ip: uploadImageResult.public_id,
            address: {
                region: req.body.address.region,
                province: req.body.address.province,
                municipality: req.body.address.municipality,
                barangay: req.body.address.barangay,
            },
            email: req.body.email,
            phone_number: req.body.phone_number,
            role: req.body.role,
            industry: req.body.industry
        })
        const token = await jwt.sign({
            id: newAPIUser._id
        }, process.env.JWT_BACKEND, {});

        let result = await mailer.sendRegistrationDataToAPIIntegration({
            username: req.body.username,
            organization_name: req.body.organization_name,
            email: req.body.email,
            phone_number: req.body.phone_number,
            role: req.body.role,
            industry: req.body.industry,
            token: token
        });

        res.status(200).send({
            status: newAPIUser,
            payload: token
        })
    } catch(err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
};