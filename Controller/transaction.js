const moment = require('moment-timezone');
const Shipping = require("../Model/schema.shipping_transaction");
const { destinationCount, shippingCost } = require("../Controller/helper/distance_calculation");
const fs = require("fs");
const path = require("path");
global.appRoot = path.resolve(`../${__dirname}`);
// const { generateAssessment } = require("../Services/Puppeteer");

let base64Encode = (file) => {
    return fs.readFileSync(file, { encoding: "base64" });
};

// Clients

exports.createTransaction = async (req, res, next) => {
    try {
        let getCost = await shippingCost(destinationCount(req.body.pick_up_address.region), destinationCount(req.body.target_address.region), req.body.products.length);
        let checkRegionEquality = destinationCount(req.body.pick_up_address.region) === destinationCount(req.body.target_address.region) ? true : false;
        let newTransaction = await Shipping.create({
            user_information: req.body.user_information,
            receiver_information: req.body.receiver_information,
            category: req.body.category,    
            pick_up_address: req.body.pick_up_address,    
            target_address: req.body.target_address,    
            products: req.body.products,    
            status: {
                sameRegion: checkRegionEquality
            },
            shippingCost: getCost,
            notes: req.body.notes
        })
        res.status(200).send({
            message: "Transaction has been created!",
            payload: newTransaction
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.viewTransactionById = async (req, res, next) => {
    try {
        let getSingleTransaction = await Shipping.findOne({ 
            _id: req.params.id
        })
        res.status(200).send({
            payload: getSingleTransaction
        })
    } catch (err) {
        console.log("this one")
        err.statusCode === undefined ? err.statusCode = 500 : '';
        console.log(err)
        return next(err);
    }
}

exports.viewTransactionInvoiceById = async (req, res, next) => {
    try {
        let getSingleTransaction = await Shipping.find({ _id: req.params.id });

        let logo = () => {
            return (
                '<img  src="data:image/png;base64,' +
                base64Encode(`Asset/InvoiceComponents/temp.jpg`) +
                '"  style="height: 80px;" />'
            );
        };

        // const pdf = await generateAssessment(`
        // <!DOCTYPE html>
        // <html lang="en">
        //     <head>
        //         <meta charset="UTF-8">
        //         <meta http-equiv="X-UA-Compatible" content="IE=edge">
        //         <meta name="viewport" content="width=device-width, initial-scale=1.0">
        //         <title>Document</title>
        //         <style>
        //         * {
        //             box-sizing:border-box
        //             padding: 0;
        //             margin: 0;
        //             font-family: Calibri, sans-serif;
        //         }
        //         @media print {
        //             .table {
        //             break-inside: avoid;
        //             }
        //         }
        //         </style>
        //     </head>
        //     <body>
        //         ${
        //             logo()
        //         }
        //     </body>
        // </html>
        // `)
        // Create Invoice Here
        res.set("Content-Type", "application/pdf");
        res.send(pdf);

        res.status(200).send(pdf)
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.cancelTransactionById = async (req, res, next) => {
    try {
        let getSingleTransaction = await Shipping.findOneAndUpdate(
            { _id: req.params.id },
            {
                $set: {
                    "status.updateText": "Shipping request is cancelled!",
                    "status.numStatus": 7,
                    "updatedAt": moment().utc("Asia/Singapore").format()
                }
            },
            {
                new: true
            }
        )
        res.status(200).send({
            payload: getSingleTransaction
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.viewTransactionHistoryByUserId = async (req, res, next) => {
    try {
        let getTransactionHistoryByUserId = await Shipping.find({ "user_information.user_id": req.params.userId })
        res.status(200).send({
            payload: getTransactionHistoryByUserId
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

// Delivery Partners

exports.getAllOpenTransactions = async (req, res, next) => {
    try {
        let newTransaction = await Shipping.find({
            "status.numStatus": 1
        })
        res.status(200).send({
            payload: newTransaction
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.acceptOpenTransaction = async (req, res, next) => {
    try {
        let checkTransaction = await Shipping.count({
            "delivery_information.delivery_partner_id": req.body.delivery_partner_id,
            "status.numStatus": {
                $gte: 2,
                $lte: 6
            }
        })
        if(checkTransaction === 3) {
            let error = new Error('You have reached the max current Transaction Delivery, please settle other Delivery Tasks before accepting!');
            error.statusCode = 501;
            throw error;
        }
        let newTransaction = await Shipping.findOneAndUpdate(
            {
                _id: req.body._id
            },
            {
                $set: {
                    "delivery_information": {
                        "delivery_partner": req.body.delivery_partner_username,
                        "delivery_partner_id": req.body.delivery_partner_id,
                        "delivery_partner_picture": req.body.delivery_partner_picture
                    },
                    "status.updateText": "Shipping request has been accepted by the Delivery Partner and will be picking up the parcel!",
                    "status.numStatus": 2
                }
            },
            {
                new: true
            }
        )
        res.status(200).send({
            message: `TS${req.body._id} has been accepted and added to your Delivery Tasks!`,
            payload: newTransaction
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getAllAcceptedCurrentTransactionsByDeliveryPartner = async (req, res, next) => {
    try {
        let newTransaction = await Shipping.find({
            "delivery_information.delivery_partner_id": req.params.userId,
            // "status.numStatus": { $nin: [ 1, 6, 7] }
        })
        res.status(200).send({
            payload: newTransaction
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}


exports.updateAcceptedTransactionStatus = async (req, res, next) => {
    try {
        let newTransaction;
        Number(req.body.numStatus) === 1 ?
            (
                newTransaction = await Shipping.findOneAndUpdate(
                    {
                        _id: req.body._id
                    },
                    {
                        $set: {
                            "status.updateText": "Shipping request is enqueued!",
                            "status.numStatus": 1,
                        },
                        $unset: {
                            "delivery_information.delivery_partner": "" ,
                            "delivery_information.delivery_partner_id": "", 
                            "delivery_information.delivery_partner_picture": "",
                            "delivery_information._id": "" 
                        }
                    },
                    {
                        new: true
                    }
                ),
                newTransaction = await Shipping.findOneAndUpdate(
                    {
                        _id: req.body._id
                    },
                    {
                        $unset: {
                            "delivery_information": "" 
                        }
                    },
                    {
                        new: true
                    }
                )
            )
        :
            newTransaction = await Shipping.findOneAndUpdate(
                {
                    _id: req.body._id
                },
                {
                    $set: {
                        "status.updateText": req.body.updateText,
                        "status.numStatus": req.body.numStatus
                    }
                },
                {
                    new: true
                }
            )
        res.status(200).send({
            message: "Transaction has been updated!",
            payload: newTransaction
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}