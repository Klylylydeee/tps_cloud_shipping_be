
const Shipping = require("../Model/schema.shipping_transaction");
const { destinationCount, shippingCost } = require("./helper/distance_calculation");

exports.requestShipping = async (req, res, next) => {
    try {
        let getCost = await shippingCost(destinationCount(req.body.pick_up_address.region), destinationCount(req.body.target_address.region), req.body.products.length);
        let checkRegionEquality = destinationCount(req.body.pick_up_address.region) === destinationCount(req.body.target_address.region) ? true : false;
        let newTransaction = await Shipping.create({
            user_information: req.body.user_information,
            receiver_information: req.body.receiver_information,
            category: req.body.category,    
            pick_up_address: req.body.pick_up_address,    
            target_address: req.body.target_address,    
            products: req.body.products,    
            status: {
                sameRegion: checkRegionEquality
            },
            shippingCost: getCost,
            notes: req.body.notes
        });
        res.status(200).send({
            payload: newTransaction
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}