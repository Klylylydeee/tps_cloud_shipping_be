const logger = require("../../Middleware/winston");
const { validationResult } = require('express-validator');

const notFound = (req, res, next) => {
    const error = new Error(`EndPoint Not Found - ${req.originalUrl}`);
    error.statusCode = 404;
    next(error);
};
  
const errorHandler = (error, req, res, next) => {
    const statusCode = error.statusCode === 502 ? 502 : error.statusCode;
    statusCode === 404 ? logger.warn(error, { metadata: error.stack }) : logger.error(error, { metadata: error.stack });
    res.status(statusCode);
    res.json({
        message: error.message,
        stack: error.stack,
    });
};

const validateRequest = (request) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        const resErrors = errors.errors.map((currError) => {
            return currError.param
        }).join(', ').concat(' is required.')
        let error = new Error(resErrors);
        error.statusCode = 400;
        throw error;
    }
}

module.exports = { notFound, errorHandler, validateRequest };