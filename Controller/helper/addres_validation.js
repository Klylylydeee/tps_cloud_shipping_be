const regions = require("../../Services/Philippines/main.json");

const checkRequestBodyAddress = async (regionData, provinceData, municipalityData, barangayData, next) => {
    try {
        console.log(regionData)
        let checkRegion = regions.some((currentData) => {
            return currentData.region_name === regionData
        })
        if(!checkRegion){
            let error = new Error('Region does not exists.');
            error.statusCode = 501;
            throw error;
        }
        let getRegionIndex = regions.map((currentData) => {
            return currentData.region_name
        }).indexOf(regionData);


        let checkProvince = Object.values(regions[getRegionIndex].provinces).some((currentData) => {
            return currentData.province_name === provinceData
        })
        if(!checkProvince){
            let error = new Error('Province does not exists.');
            error.statusCode = 501;
            throw error;
        }
        let getProvinceIndex = Object.values(regions[getRegionIndex].provinces).map((currentData) => {
            return currentData.province_name
        }).indexOf(provinceData);

        
        let checkMunicipality = Object.values(regions[getRegionIndex].provinces[getProvinceIndex].municipalities).some((currentData) => {
            return currentData.municipality_name === municipalityData
        })
        if(!checkMunicipality){
            let error = new Error('Municipality does not exists.');
            error.statusCode = 501;
            throw error;
        }
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

module.exports = { checkRequestBodyAddress };