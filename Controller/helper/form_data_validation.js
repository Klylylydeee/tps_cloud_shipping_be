exports.authSignIn = async (req, res, next) => {
    const validateFields = [
        "first_name",
        "last_name",
        "username",
        "address",
        "email",
        "phone_number",
        "role",
        "password",
    ];
    let validateFormDataBody = JSON.parse(JSON.stringify(req.body));
    let validationResult = validateFields.map((currentDataField) => {
        return Object.keys(validateFormDataBody).some((currentField) => {
            return currentDataField == currentField;
        });
    });
    let getFailedIndexes = [];
    let failedIndex = false;
    validationResult.some((currentIndex, failIndex) => {
        if (currentIndex === false) {
            failedIndex = true;
            getFailedIndexes.push(failIndex);
        }
    });
    if (failedIndex) {
        const resErrors = getFailedIndexes
            .map((currentIndex) => {
                return validateFields[currentIndex];
            })
            .join(", ")
            .concat(" is required.");
        const error = new Error(resErrors);
        error.statusCode = 400;
        throw next(error);
    }
    next();
};

exports.integrationSignUp = async (req, res, next) => {
    const validateFields = [
        "username",
        "organization_name",
        "address",
        "email",
        "phone_number",
        "role",
        "industry"
    ];
    let validateFormDataBody = JSON.parse(JSON.stringify(req.body));
    let validationResult = validateFields.map((currentDataField) => {
        return Object.keys(validateFormDataBody).some((currentField) => {
            return currentDataField == currentField;
        });
    });
    let getFailedIndexes = [];
    let failedIndex = false;
    validationResult.some((currentIndex, failIndex) => {
        if (currentIndex === false) {
            failedIndex = true;
            getFailedIndexes.push(failIndex);
        }
    });
    if (failedIndex) {
        const resErrors = getFailedIndexes
            .map((currentIndex) => {
                return validateFields[currentIndex];
            })
            .join(", ")
            .concat(" is required.");
        const error = new Error(resErrors);
        error.statusCode = 400;
        throw next(error);
    }
    next();
};
