const destinationCount = (setRegion) => {
    switch (setRegion) {
        case "REGION I":
            return 1;
        case "CAR":
            return 2;
        case "REGION II":
            return 3;
        case "REGION III":
            return 4;
        case "NCR":
            return 5;
        case "REGION IV-A":
            return 6;
        case "REGION IV-B":
            return 7;
        case "REGION V":
            return 8;
        case "REGION VI":
            return 9;
        case "REGION VIII":
            return 10;
        case "REGION VII":
            return 11;
        case "REGION IX":
            return 12;
        case "REGION X":
            return 13;
        case "REGION XIII":
            return 14;
        case "REGION XI":
            return 15;
        case "REGION XII":
            return 16;
        case "BARMM":
            return 17;
    }
};

const shippingCost = (startingLocation, endLocation, productLength) => {
    let shippingFee = 50 * productLength;
    let estimateShippingFee;
    if (startingLocation > endLocation) {
        let currentEstimate = (10 * startingLocation) / endLocation;
        estimateShippingFee = currentEstimate * productLength;
    } else {
        let currentEstimate = (10 * endLocation) / startingLocation;
        estimateShippingFee = currentEstimate * productLength;
    }
    return estimateShippingFee >= 750
        ? 750
        : estimateShippingFee <= shippingFee
        ? shippingFee
        : estimateShippingFee;
};

module.exports = { destinationCount, shippingCost };