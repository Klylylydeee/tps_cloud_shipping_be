const random = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

const getLeocationValue =  (province) => {
    let langtitude, longtitude;
    switch(province){
        case "ABRA":
            langtitude = `17.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "AGUSAN DEL NORTE":
            langtitude = `9.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "AGUSAN DEL SUR":
            langtitude = `8.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `126.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "AKLAN":
            langtitude = `11.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ALBAY":
            langtitude = `13.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ANTIQUE":
            langtitude = `11.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "APAYAO":
            langtitude = `18.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "AURORA":
            langtitude = `14.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BASILAN":
            langtitude = `10.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BATAAN":
            langtitude = `14.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BATANES":
            langtitude = `20.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BATANGAS":
            langtitude = `14.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BENGUET":
            langtitude = `16.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BILIRAN":
            langtitude = `11.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BOHOL":
            langtitude = `9.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BUKIDNON":
            langtitude = `8.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "BULACAN":
            langtitude = `14.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CAGAYAN":
            langtitude = `18.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CAMARINES NORTE":
            langtitude = `14.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.7${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CAMARINES SUR":
            langtitude = `13.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CAMIGUIN":
            langtitude = `18.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CAPIZ":
            langtitude = `11.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CATANDUANES":
            langtitude = `17.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CAVITE":
            langtitude = `14.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "CEBU":
            langtitude = `10.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "COMPOSTELA VALLEY":
            langtitude = `7.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `126.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "COTABATO (NORTH COT.)":
            langtitude = `7.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "DAVAO (DAVAO DEL NORTE)":
            langtitude = `7.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "DAVAO DEL SUR":
            langtitude = `7.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "DAVAO OCCIDENTAL":
            langtitude = `7.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "DAVAO ORIENTAL":
            langtitude = `7.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `126.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "DINAGAT ISLANDS":
            langtitude = `10.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "EASTERN SAMAR":
            langtitude = `12.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "GUIMARAS":
            langtitude = `10.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "IFUGAO":
            langtitude = `16.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ILOCOS NORTE":
            langtitude = `18.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.7${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ILOCOS SUR":
            langtitude = `17.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ILOILO":
            langtitude = `10.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ISABELA":
            langtitude = `16.7${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "KALINGA":
            langtitude = `17.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "LA UNION":
            langtitude = `6.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `126.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "LAGUNA":
            langtitude = `14.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "LANAO DEL NORTE":
            langtitude = `8.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "LANAO DEL SUR":
            langtitude = `7.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "LEYTE":
            langtitude = `10.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "MAGUINDANAO":
            langtitude = `6.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "MARINDUQUE":
            langtitude = `13.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "MASBATE":
            langtitude = `12.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "MISAMIS OCCIDENTAL":
            langtitude = `8.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "MISAMIS ORIENTAL":
            langtitude = `17.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "MOUNTAIN PROVINCE":
            langtitude = `16.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NATIONAL CAPITAL REGION - FOURTH DISTRICT":
            langtitude = `14.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NATIONAL CAPITAL REGION - MANILA":
            langtitude = `14.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NATIONAL CAPITAL REGION - SECOND DISTRICT":
            langtitude = `14.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NATIONAL CAPITAL REGION - THIRD DISTRICT":
            langtitude = `14.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NEGROS OCCIDENTAL":
            langtitude = `10.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NEGROS ORIENTAL":
            langtitude = `10.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NORTHERN SAMAR":
            langtitude = `12.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NUEVA ECIJA":
            langtitude = `15.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "NUEVA VIZCAYA":
            langtitude = `16.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "OCCIDENTAL MINDORO":
            langtitude = `13.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.7${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ORIENTAL MINDORO":
            langtitude = `13.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "PALAWAN":
            langtitude = `9.7${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `118.7${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "PAMPANGA":
            langtitude = `15.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "PANGASINAN":
            langtitude = `15.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "QUEZON":
            langtitude = `14.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "QUIRINO":
            langtitude = `17.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.7${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "RIZAL":
            langtitude = `12.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ROMBLON":
            langtitude = `12.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SAMAR (WESTERN SAMAR)":
            langtitude = `11.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SARANGANI":
            langtitude = `6.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SIQUIJOR":
            langtitude = `9.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SORSOGON":
            langtitude = `17.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SOUTH COTABATO":
            langtitude = `6.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SOUTHERN LEYTE":
            langtitude = `10.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SULTAN KUDARAT":
            langtitude = `7.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `124.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SULU":
            langtitude = `6.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SURIGAO DEL NORTE":
            langtitude = `9.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `125.6${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "SURIGAO DEL SUR":
            langtitude = `8.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `126.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "TAGUIG - PATEROS":
            langtitude = `14.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `121.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "TARLAC":
            langtitude = `15.4${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "TAWI-TAWI":
            langtitude = `5.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `119.5${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ZAMBALES":
            langtitude = `14.8${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `120.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ZAMBOANGA DEL NORTE":
            langtitude = `8.3${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.1${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ZAMBOANGA DEL SUR":
            langtitude = `7.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `123.2${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        case "ZAMBOANGA SIBUGAY":
            langtitude = `6.9${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            longtitude = `122.0${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            break;
        default: 
            doc.latitude = `12.${random(0, 9)}${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
            doc.longitude = `122.${random(0, 9)}${random(0, 9)}${random(0, 9)}${random(0, 9)}`;
    }
    return { langtitude, longtitude }
}

module.exports = { getLeocationValue, random }