const User = require("../Model/schema.user");
const Shipping = require("../Model/schema.shipping_transaction");
const moment = require("moment")

exports.getListOfAllUserWithFullDetails = async (req, res, next) => {
    try { 
        let userList = await User.find({}, { password: 0, updatedAt: 0, __v: 0, cloudinary_public_ip: 0 });

        let numberOfClients = userList.filter((currentUser) => {
            return currentUser.role === "Client"
        })

        let numberOfDeliveryPartners = userList.filter((currentUser) => {
            return currentUser.role === "Delivery Partner"
        })

        let numberOfAPIIntegration = userList.filter((currentUser) => {
            return currentUser.role === "API Integration Partner"
        })

        res.status(200).send({
            message: `Total numbers of Client, Delivery Partners and API Integration users: ${userList.length}`,
            payload: {
                count: {
                    clients: numberOfClients.length,
                    delivery_partners: numberOfDeliveryPartners.length,
                    api_integration: numberOfAPIIntegration.length
                },
                list: {
                    clients: numberOfClients,
                    delivery_partners: numberOfDeliveryPartners,
                    api_integration: numberOfAPIIntegration
                },
                all: userList
            }
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getListOfAllUserBasedOnDynamicRole = async (req, res, next) => {
    try { 
        let userList = await User.find({ role: {'$regex' : `^${req.query.role}$`, '$options' : 'i'} }, { password: 0, updatedAt: 0, __v: 0, cloudinary_public_ip: 0 });

        res.status(200).send({
            message: `Total numbers of ${req.query.role} users: ${userList.length}`,
            payload: {
                count: userList.length,
                list: userList
            }
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getListOfAllTransaction = async (req, res, next) => {
    try { 
        let transactionList = await Shipping.find({}, { updatedAt: 0, __v: 0, _id: 0 });

        res.status(200).send({
            message: `Total numbers of Transactions: ${transactionList.length}`,
            payload: {
                count: transactionList.length,
                list: transactionList
            }
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getSumCostOfAllTransactionBySpecificMonth = async (req, res, next) => {
    try { 
        let transactionList = await Shipping.aggregate([
            {
                $match: {
                    createdAt: {
                        $gte: new Date(`${req.query.year}-${req.query.month}-01T00:00:00.0Z`),
                        $lt: new Date(`${req.query.year}-${req.query.month}-31T15:58:26.000Z`)
                    }
                }
            },
            { 
                // To be selected fields in the Documents
                $project: {
                    shippingCost: 1,
                    // Create field named Month based on createdAt Month value 
                    month: { 
                        $month: "$createdAt" 
                    }
                }
            }, 
            { 
                // Create array that seperates each Month's value
                $group: {
                    _id: "$month", 
                    // Sums all cost of data with same month field value
                    total: {
                        $sum: "$shippingCost"
                    }
                }
            },
            { 
                $sort: { 
                    _id : 1 
                }
            },
            {
                $unset: "_id"
            }
        ])

        let getTotal = transactionList[0] ? transactionList[0].total : 0;
        let getMonth = moment(`${req.query.year} ${req.query.month}`).format("MMMM")

        res.status(200).send({
            message: `Total sum for ${getMonth} ${req.query.year}`,
            payload: getTotal
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getSumCostOfAllTransactionByMonth = async (req, res, next) => {
    try { 
        let transactionList = await Shipping.aggregate([
            {
                $match: {
                    createdAt: {
                        $gte: new Date(`${req.query.year}-01-01T00:00:00.0Z`),
                        $lt: new Date(`${req.query.year}-12-31T15:58:26.000Z`)
                    }
                }
            },
            { 
                // To be selected fields in the Documents
                $project: {
                    shippingCost: 1,
                    // Create field named Month based on createdAt Month value 
                    month: { 
                        $month: "$createdAt" 
                    }
                }
            }, 
            { 
                // Create array that seperates each Month's value
                $group: {
                    _id: "$month", 
                    // Sums all cost of data with same month field value
                    total: {
                        $sum: "$shippingCost"
                    }
                }
            },
            { 
                $sort: { 
                    _id : 1 
                }
            },
            {
                // Due to new date, the fetching of data starts at 0 = january, this add field would return the query back to
                // january === 1
                $addFields: {
                    id: {
                        $let: {
                            vars: {
                                monthsInString: [
                                    0,
                                    1,
                                    2,
                                    3,
                                    4,
                                    5,
                                    6,
                                    7,
                                    8,
                                    9,
                                    10,
                                    11,
                                    12
                                ]
                            },
                            in: {
                                $arrayElemAt: [
                                    '$$monthsInString',
                                    '$_id'
                                ]
                            }
                        }
                    }
                }
            },
            {
                // Create new Field based on id, id can be 1-12 based on the Month int to string conversion
                $addFields: {
                    month: {
                        $let: {
                            vars: {
                                monthsInString: [
                                    // don't mind this, it's just for index spacing to callibrate back to january
                                    '',
                                    'January',
                                    'February',
                                    'March',
                                    'April',
                                    'May ',
                                    'June',
                                    'July',
                                    'August',
                                    'September',
                                    'October',
                                    'November',
                                    'December'
                                ]
                            },
                            in: {
                                $arrayElemAt: [
                                    '$$monthsInString',
                                    '$id'
                                ]
                            }
                        }
                    }
                }
            },
            { 
                $unset: "_id"
            },
            { 
                $unset: "id"
            }
        ])

        let getTotalCost = transactionList.map((currentData) => {
            return currentData.total
        }).reduce((currentValue, additionalValue) => currentValue + additionalValue, 0)

        res.status(200).send({
            message: `List of total cost per Month of the Year ${req.query.year}`,
            payload: {
                total_cost: getTotalCost,
                transactions: transactionList
            }
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getSumCostOfAllTransactionByYear = async (req, res, next) => {
    try { 
        let transactionList = await Shipping.aggregate([
            { 
                // To be selected fields in the Documents
                $project: {
                    shippingCost: 1,
                    // Create field named Month based on createdAt Month value 
                    year: { 
                        $year: "$createdAt" 
                    }
                }
            }, 
            { 
                // Create array that seperates each Month's value
                $group: {
                    _id: "$year", 
                    // Sums all cost of data with same month field value
                    total: {
                        $sum: "$shippingCost"
                    }
                }
            },
            { 
                $sort: { 
                    _id : 1 
                }
            },
            {
                // Create new Field based on _id, _id can be 1-12 based on the Month int to string conversion
                $addFields: {
                    year: "$_id"
                }
            },
            {
                $unset: "_id"
            }
        ])

        res.status(200).send({
            message: `List of total cost per Month of the Year ${new Date().getFullYear()}`,
            payload: transactionList
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getAVGCostOfAllTransactionByMonth = async (req, res, next) => {
    try { 
        let transactionList = await Shipping.aggregate([
            {
                $match: {
                    createdAt: {
                        $gte: new Date(`${req.query.year}-01-01T00:00:00.0Z`),
                        $lt: new Date(`${req.query.year}-12-31T15:58:26.000Z`)
                    }
                }
            },
            { 
                // To be selected fields in the Documents
                $project: {
                    shippingCost: 1,
                    // Create field named Month based on createdAt Month value 
                    month: { 
                        $month: "$createdAt" 
                    }
                }
            }, 
            { 
                // Create array that seperates each Month's value
                $group: {
                    _id: "$month", 
                    // Sums all cost of data with same month field value
                    average_cost: {
                        $avg: "$shippingCost"
                    }
                }
            },
            { 
                $sort: { 
                    _id : 1 
                }
            },
            {
                // Due to new date, the fetching of data starts at 0 = january, this add field would return the query back to
                // january === 1
                $addFields: {
                    id: {
                        $let: {
                            vars: {
                                monthsInString: [
                                    0,
                                    1,
                                    2,
                                    3,
                                    4,
                                    5,
                                    6,
                                    7,
                                    8,
                                    9,
                                    10,
                                    11,
                                    12
                                ]
                            },
                            in: {
                                $arrayElemAt: [
                                    '$$monthsInString',
                                    '$_id'
                                ]
                            }
                        }
                    }
                }
            },
            {
                // Create new Field based on id, id can be 1-12 based on the Month int to string conversion
                $addFields: {
                    month: {
                        $let: {
                            vars: {
                                monthsInString: [
                                    // don't mind this, it's just for index spacing to callibrate back to january
                                    '',
                                    'January',
                                    'February',
                                    'March',
                                    'April',
                                    'May ',
                                    'June',
                                    'July',
                                    'August',
                                    'September',
                                    'October',
                                    'November',
                                    'December'
                                ]
                            },
                            in: {
                                $arrayElemAt: [
                                    '$$monthsInString',
                                    '$id'
                                ]
                            }
                        }
                    }
                }
            },
            { 
                $unset: "_id"
            },
            { 
                $unset: "id"
            }
        ])

        let roundedOffValues = transactionList.map((currentData) => {
            return {...currentData, average_cost: Number(currentData.average_cost.toFixed(2))}
        });

        res.status(200).send({
            message: `List of average cost per Month of the Year ${req.query.year}`,
            payload: roundedOffValues
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getAVGCostOfAllTransactionByYear = async (req, res, next) => {
    try { 
        let transactionList = await Shipping.aggregate([
            { 
                // To be selected fields in the Documents
                $project: {
                    shippingCost: 1,
                    // Create field named Month based on createdAt Month value 
                    year: { 
                        $year: "$createdAt" 
                    }
                }
            }, 
            { 
                // Create array that seperates each Month's value
                $group: {
                    _id: "$year", 
                    // Sums all cost of data with same month field value
                    average_cost: {
                        $avg: "$shippingCost"
                    }
                }
            },
            { 
                $sort: { 
                    _id : 1 
                }
            },
            {
                // Create new Field based on _id, _id can be 1-12 based on the Month int to string conversion
                $addFields: {
                    year: "$_id"
                }
            },
            {
                $unset: "_id"
            }
        ])

        let roundedOffValues = transactionList.map((currentData) => {
            return {...currentData, average_cost: Number(currentData.average_cost.toFixed(2))}
        });

        res.status(200).send({
            message: `List of Average Cost per Year`,
            payload: roundedOffValues
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getTransactionListOfUser = async (req, res, next) => {
    try { 
        let transactionList = await Shipping.find(
            {
                "user_information.user_id": req.query.id
            },
            {
                shippingCost: 1,
                _id: 1,
                createdAt: 1
            }
        );

        let totalCost = transactionList.map((currentData) => {
            return currentData.shippingCost
        }).reduce((currentValue, additionalValue) => currentValue + additionalValue, 0)

        res.status(200).send({
            message: `Transaction list of User: ${req.query.id}`,
            payload: {
                total_cost: totalCost,
                transactions: transactionList
            }
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getAllUserAndMonthlyData = async (req, res, next) => {
    try {

        let monthlyAnalysisData = [];

        let userList = await User.find(
            {}, "_id"
        );
        for(let currentUser of userList){
            monthlyAnalysisData.push({
                user: currentUser._id.toString(),
                cost: {
                    January: 0,
                    February: 0,
                    March: 0,
                    April: 0,
                    May: 0,
                    June: 0,
                    July: 0,
                    August: 0,
                    September: 0,
                    October: 0,
                    November: 0,
                    December: 0
                }
            })
        };

        let transactionList = await Shipping.find(
            {
                createdAt: {
                    $gte: new Date(`${req.query.year}-01-01T00:00:00.0Z`),
                    $lt: new Date(`${req.query.year}-12-31T15:58:26.000Z`)
                }
            }, "user_information.user_id shippingCost createdAt"
        );
        let filteredTransaction = transactionList.map((currentData) => {
            return {
                id: currentData.user_information.user_id.toString(),
                cost: currentData.shippingCost,
                date: moment(currentData.createdAt).format("MMMM")
            }
        });
        for(let currentUser of filteredTransaction){
            if(
                monthlyAnalysisData.some((currentID) => {
                    return currentUser.id === currentID.user
                })
            ){
                let indexOfUserInArray = monthlyAnalysisData.findIndex((currentID) => {
                    return currentUser.id === currentID.user
                });
                monthlyAnalysisData[indexOfUserInArray].cost[`${currentUser.date}`] += currentUser.cost
            }
        }

        res.status(200).send({
            message: `All User Monthly Transaction Cost Data Table Graph of Year ${req.query.year}`,
            user_transactions: monthlyAnalysisData
        });

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}