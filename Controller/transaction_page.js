const Shipping = require("../Model/schema.shipping_transaction");

exports.transactionForUser = async (req, res, next) => {
    try {
        
        let getTransactionHistoryOfUser = await Shipping.find(
            { 
                "user_information.user_id": req.body.user_id
            }
        ).select({ 
            user_information: 0,
            pick_up_address: 0,
            updatedAt: 0,
            products: 0,
            pick_up_address: 0,
            notes: 0,
            __v: 0
        })

        let transactionList = await Shipping.aggregate([
            {
                $match: {
                    "user_information.user_id": req.body.user_id,
                    createdAt: {
                        $gte: new Date(`${req.body.year}-01-01T00:00:00.0Z`),
                        $lt: new Date(`${req.body.year}-12-31T15:58:26.000Z`)
                    }
                }
            },
            { 
                // To be selected fields in the Documents
                $project: {
                    shippingCost: 1,
                    // Create field named Month based on createdAt Month value 
                    month: { 
                        $month: "$createdAt" 
                    }
                }
            }, 
            { 
                // Create array that seperates each Month's value
                $group: {
                    _id: "$month", 
                    // Sums all cost of data with same month field value
                    total: {
                        $sum: "$shippingCost"
                    }
                }
            },
            { 
                $sort: { 
                    _id : 1 
                }
            },
            {
                // Due to new date, the fetching of data starts at 0 = january, this add field would return the query back to
                // january === 1
                $addFields: {
                    id: {
                        $let: {
                            vars: {
                                monthsInString: [
                                    0,
                                    1,
                                    2,
                                    3,
                                    4,
                                    5,
                                    6,
                                    7,
                                    8,
                                    9,
                                    10,
                                    11,
                                    12
                                ]
                            },
                            in: {
                                $arrayElemAt: [
                                    '$$monthsInString',
                                    '$_id'
                                ]
                            }
                        }
                    }
                }
            },
            {
                // Create new Field based on id, id can be 1-12 based on the Month int to string conversion
                $addFields: {
                    month: {
                        $let: {
                            vars: {
                                monthsInString: [
                                    // don't mind this, it's just for index spacing to callibrate back to january
                                    '',
                                    'January',
                                    'February',
                                    'March',
                                    'April',
                                    'May ',
                                    'June',
                                    'July',
                                    'August',
                                    'September',
                                    'October',
                                    'November',
                                    'December'
                                ]
                            },
                            in: {
                                $arrayElemAt: [
                                    '$$monthsInString',
                                    '$id'
                                ]
                            }
                        }
                    }
                }
            },
            { 
                $unset: "_id"
            },
            { 
                $unset: "id"
            }
        ])

        let newTransactionList = [
            'January',
            'February',
            'March',
            'April',
            'May ',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ].map((currentMap) => {
            if(transactionList.some((currentTransactionMonth) => {
                return currentTransactionMonth.month === currentMap
            })) {
                let getIndexData = transactionList.map((currentTransactionMonth) => {
                    return currentTransactionMonth.month
                }).indexOf(currentMap)
                return transactionList[getIndexData]
            } else {
                return {
                    "total": 0,
                    "month": currentMap
                }
            }
        })

        res.status(200).send({
            transaction_history: getTransactionHistoryOfUser,
            transaction_by_month: newTransactionList
        })

    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}