const User = require("../Model/schema.user");
const cloudinary = require("../Services/Cloudinary");

exports.addLicense = async (req, res, next) => {
    try {
        let updateUserLicense = await User.findOneAndUpdate(
            {
                _id: req.body.id
            },
            {
                $set: {
                    licensed_no: req.body.licensed_no
                }
            },
            {
                new: true
            }
        )
        res.status(200).send({
            payload: updateUserLicense
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}
exports.addVehicle = async (req, res, next) => {
    try {
        if(!req.file){
            throw err = new Error("Missing image in the request body.")
        }
        const uploadImageResult = await cloudinary.uploader.unsigned_upload(req.file.path, 'cloud-shipping');
        if(!uploadImageResult){
            throw err = new Error("Upload failed")
        }
        let updateUserLicense = await User.findOneAndUpdate(
            {
                _id: req.body.id
            },
            {
                $push: {
                    vehicles: {
                        vehicle_type: req.body.vehicle_type,
                        plate_no: req.body.plate_no,
                        vehicle_model: req.body.vehicle_model,
                        vehicle_cloudinary_public_ip: uploadImageResult.secure_url,
                        cloudinary_public_ip: uploadImageResult.public_id
                    }
                }
            },
            {
                new: true
            }
        )
        res.status(200).send({
            payload: updateUserLicense
        })
    } catch (err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}