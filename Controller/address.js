const regions = require("../Services/Philippines/main.json");

exports.getRegion = async (req, res, next) => {
    try {
        let regionList = regions.map((currentData) => {
            return currentData.region_name
        });
        res.status(200).send({
            message: 'Lists of Regions in the Philippines',
            payload: regionList
        });
    } catch(err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getProvinces = async (req, res, next) => {
    try {
        let checkRegion = regions.some((currentData) => {
            return currentData.region_name === req.params.region
        })
        
        if(!checkRegion){
            let error = new Error('Region does not exists.');
            error.statusCode = 501;
            throw error;
        }

        let getRegionIndex = regions.map((currentData) => {
            return currentData.region_name
        }).indexOf(req.params.region);

        let getProvinces = Object.values(regions[getRegionIndex].provinces).map((currentData) => {
            return currentData.province_name
        });

        res.status(200).send({
            message: `Lists of Provinces in the Region of ${req.params.region}`,
            payload: getProvinces
        });

    } catch(err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getMunicipality = async (req, res, next) => {
    try {
        let checkRegion = regions.some((currentData) => {
            return currentData.region_name === req.params.region
        })
        if(!checkRegion){
            let error = new Error('Region does not exists.');
            error.statusCode = 501;
            throw error;
        }
        let getRegionIndex = regions.map((currentData) => {
            return currentData.region_name
        }).indexOf(req.params.region);


        let checkProvince = Object.values(regions[getRegionIndex].provinces).some((currentData) => {
            return currentData.province_name === req.params.province
        })
        if(!checkProvince){
            let error = new Error('Province does not exists.');
            error.statusCode = 501;
            throw error;
        }
        let getProvinceIndex = Object.values(regions[getRegionIndex].provinces).map((currentData) => {
            return currentData.province_name
        }).indexOf(req.params.province);

        let getMunicipalities = Object.values(regions[getRegionIndex].provinces[getProvinceIndex].municipalities).map((currentData) => {
            return currentData.municipality_name
        });

        res.status(200).send({
            message: `Lists of Municipalities in the Region of ${req.params.region}, Province of ${req.params.province}`,
            payload: getMunicipalities
        });

    } catch(err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}

exports.getBarangays = async (req, res, next) => {
    try {
        let checkRegion = regions.some((currentData) => {
            return currentData.region_name === req.params.region
        })
        if(!checkRegion){
            let error = new Error('Region does not exists.');
            error.statusCode = 501;
            throw error;
        }
        let getRegionIndex = regions.map((currentData) => {
            return currentData.region_name
        }).indexOf(req.params.region);


        let checkProvince = Object.values(regions[getRegionIndex].provinces).some((currentData) => {
            return currentData.province_name === req.params.province
        })
        if(!checkProvince){
            let error = new Error('Province does not exists.');
            error.statusCode = 501;
            throw error;
        }
        let getProvinceIndex = Object.values(regions[getRegionIndex].provinces).map((currentData) => {
            return currentData.province_name
        }).indexOf(req.params.province);

        
        let checkMunicipality = Object.values(regions[getRegionIndex].provinces[getProvinceIndex].municipalities).some((currentData) => {
            return currentData.municipality_name === req.params.municipality
        })
        if(!checkMunicipality){
            let error = new Error('Municipality does not exists.');
            error.statusCode = 501;
            throw error;
        }
        let getMunicipalityIndex = Object.values(regions[getRegionIndex].provinces[getProvinceIndex].municipalities).map((currentData) => {
            return currentData.municipality_name
        }).indexOf(req.params.municipality);

        let getBarangay = regions[getRegionIndex].provinces[getProvinceIndex].municipalities[getMunicipalityIndex].barangays

        res.status(200).send({
            message: `Lists of Barangays in the Region of ${req.params.region}, Province of ${req.params.province}, Municipality of ${req.params.municipality}`,
            payload: getBarangay
        });

    } catch(err) {
        err.statusCode === undefined ? err.statusCode = 500 : '';
        return next(err);
    }
}