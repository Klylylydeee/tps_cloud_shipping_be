const jwt = require("jsonwebtoken");

exports.getAuthorization = (req, res, next) => {
    const authHeader = req.get("Authorization");

    if (authHeader === undefined) {
        const error = new Error(
            "Authorization does not exist! Please Sign-in!"
        );
        error.statusCode = 403;
        throw next(error);
    }

    const token =
        authHeader.split(" ")[0] === "Bearer" ? authHeader.split(" ")[1] : "";
    let decodedToken;

    try {
        decodedToken = jwt.verify(token, process.env.JWT_BACKEND);
    } catch (err) {
        throw new Error(err.message);
    }

    if (!decodedToken) {
        const error = new Error("Not Authenticated");
        error.statusCode = 401;
        throw next(error);
    }

    if (decodedToken.role === "API Integration Partner") {
        let error = new Error(
            "Only for regular and business clients. Please use the integration API instead!"
        );
        error.statusCode = 501;
        throw error;
    }

    next();
};

exports.getIntegrationAuthorization = (req, res, next) => {
    const authHeader = req.get("Authorization");

    if (authHeader === undefined) {
        const error = new Error(
            "Authorization does not exist! Please Sign-in!"
        );
        error.statusCode = 403;
        throw next(error);
    }

    const token =
        authHeader.split(" ")[0] === "Bearer" ? authHeader.split(" ")[1] : "";
    let decodedToken;

    try {
        decodedToken = jwt.verify(token, process.env.JWT_BACKEND);
    } catch (err) {
        err.statusCode = 500;
        throw err;
    }

    if (!decodedToken) {
        const error = new Error("Not Authenticated");
        error.statusCode = 401;
        throw next(error);
    }

    if (decodedToken.role !== "API Integration Partner") {
        let error = new Error(
            "Only for integrated business partners. Please register before using this service!"
        );
        error.statusCode = 501;
        throw error;
    }

    next();
};

exports.getAnalyticsIntegration = (req, res, next) => {
    const roleHeader = req.get("TPSRole");
    const authHeader = req.get("TPSAuthenticate");

    if (authHeader === undefined) {
        const error = new Error(
            "Authorization error! Please communicate with the TPS Cloud Shipping Web Administrator!"
        );
        error.statusCode = 403;
        throw next(error);
    }

    if(roleHeader !== process.env.ANALYTICS_SECRET || roleHeader === undefined){
        const error = new Error(
            "Header error! Please communicate with the TPS Cloud Shipping Web Administrator!"
        );
        error.statusCode = 403;
        throw next(error);
    }

    const token =
        authHeader.split(" ")[0] === "Handler" ? authHeader.split(" ")[1] : "";
    let decodedToken;

    try {
        decodedToken = jwt.verify(token, process.env.JWT_BACKEND);
    } catch (err) {
        err.statusCode = 500;
        throw err;
    }

    if (!decodedToken) {
        const error = new Error("Not Authenticated");
        error.statusCode = 401;
        throw next(error);
    }

    next();
};