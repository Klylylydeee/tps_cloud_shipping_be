const express = require("express");
const router = express.Router();
const analyticsController = require("../Controller/analytics");
const jwtAuthentication = require("../Middleware/jsonwebtoken");

router.get(
    "/users",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getListOfAllUserWithFullDetails
);

router.get(
    "/users/query",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getListOfAllUserBasedOnDynamicRole
);

router.get(
    "/transactions",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getListOfAllTransaction
);

router.get(
    "/transactions/user",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getTransactionListOfUser
);

router.get(
    "/transactions/cost/monthly/query",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getSumCostOfAllTransactionBySpecificMonth
);

router.get(
    "/transactions/cost/monthly",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getSumCostOfAllTransactionByMonth
);

router.get(
    "/transactions/cost/yearly",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getSumCostOfAllTransactionByYear
);

router.get(
    "/transactions/avg-cost/monthly",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getAVGCostOfAllTransactionByMonth
);

router.get(
    "/transactions/avg-cost/yearly",
    jwtAuthentication.getAnalyticsIntegration,
    analyticsController.getAVGCostOfAllTransactionByYear
);

router.get(
    "/transactions/graph",
    analyticsController.getAllUserAndMonthlyData
)

module.exports = router;
