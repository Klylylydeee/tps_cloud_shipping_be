const express = require("express");
const router = express.Router();
const transactionPageController = require("../Controller/transaction_page");

router.post('/transaction/user',  transactionPageController.transactionForUser);
  
module.exports = router;