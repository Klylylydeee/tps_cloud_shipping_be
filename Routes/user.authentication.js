const express = require("express");
const multer = require("multer");
const router = express.Router();
const { check } = require('express-validator');
const authController = require("../Controller/authentication");
const FormDataController = require("../Controller/helper/form_data_validation");
const storage = require("../Services/Multer");
const upload = multer({ storage: storage });
// const cloudinary = require("../Services/Cloudinary");

router.post('/sign-in', [
    check('username').not().isEmpty(),
    check('password').not().isEmpty(),
], authController.authSignIn);

router.post('/sign-up', upload.single("user_picture"), FormDataController.authSignIn, authController.authSignUp);

router.post('/integration/sign-up', upload.single("user_picture"), FormDataController.integrationSignUp, authController.integrationSignUp);

// router.delete('/delete', async (req, res, next)=>{
//     let result = await cloudinary.uploader.destroy("cloud-shipping/x9umwrsjvxza6zfzuwal");
//     res.send(result)
// })
  
module.exports = router;