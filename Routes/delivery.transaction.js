const express = require("express");
const multer = require("multer");
const router = express.Router();
const { check } = require('express-validator');
const transactionController = require("../Controller/transaction");
const deliveryPartnerInformationController = require("../Controller/partner_information");
const storage = require("../Services/Multer");
const upload = multer({ storage: storage });
const jwtAuthentication = require("../Middleware/jsonwebtoken");

router.get('/open-transactions', jwtAuthentication.getAuthorization, transactionController.getAllOpenTransactions);

router.patch('/accept-transaction/', jwtAuthentication.getAuthorization, transactionController.acceptOpenTransaction);

router.get('/current-transaction/:userId', jwtAuthentication.getAuthorization, transactionController.getAllAcceptedCurrentTransactionsByDeliveryPartner);

router.patch('/update-transaction/', jwtAuthentication.getAuthorization, transactionController.updateAcceptedTransactionStatus);

router.patch('/license/add', jwtAuthentication.getAuthorization, deliveryPartnerInformationController.addLicense);

router.patch('/vehicle/add', jwtAuthentication.getAuthorization, upload.single("user_picture"), deliveryPartnerInformationController.addVehicle);
  
module.exports = router;