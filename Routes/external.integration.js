const express = require("express");
const router = express.Router();
const { check } = require('express-validator');
const integrationController = require("../Controller/integration");
const transactionController = require("../Controller/transaction");
const addressController = require("../Controller/address");
const jwtAuthentication = require("../Middleware/jsonwebtoken");

router.post('/transaction/create', jwtAuthentication.getIntegrationAuthorization, integrationController.requestShipping);

router.get('/transaction/:id', jwtAuthentication.getIntegrationAuthorization, transactionController.viewTransactionById);

router.get('/transaction/invoice/:id', jwtAuthentication.getIntegrationAuthorization, transactionController.viewTransactionInvoiceById);

router.patch('/transaction/:id', jwtAuthentication.getIntegrationAuthorization, transactionController.cancelTransactionById);

router.get('/transaction/all/:userId', jwtAuthentication.getIntegrationAuthorization, transactionController.viewTransactionHistoryByUserId);

router.get('/address/', addressController.getRegion);

router.get('/address/:region', addressController.getProvinces);

router.get('/address/:region/:province', addressController.getMunicipality);

router.get('/address/:region/:province/:municipality', addressController.getBarangays);

module.exports = router;