const express = require("express");
const router = express.Router();
const { check } = require('express-validator');
const transactionController = require("../Controller/transaction");
const jwtAuthentication = require("../Middleware/jsonwebtoken");

router.post('/transaction/create', jwtAuthentication.getAuthorization, transactionController.createTransaction);

router.get('/transaction/:id', jwtAuthentication.getAuthorization, transactionController.viewTransactionById);

router.get('/transaction/invoice/:id', jwtAuthentication.getAuthorization, transactionController.viewTransactionInvoiceById);

router.patch('/transaction/:id', jwtAuthentication.getAuthorization, transactionController.cancelTransactionById);

router.get('/transaction/all/:userId', jwtAuthentication.getAuthorization, transactionController.viewTransactionHistoryByUserId);
  
module.exports = router;