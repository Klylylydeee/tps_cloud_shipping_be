const express = require("express");
const dotenv = require("dotenv").config({ path: "./Configuration/config.env" });
const cors = require("cors");
const logger = require("./Middleware/winston");
const morgan = require("morgan");
const helmet = require("helmet");
const rateLimit = require("express-rate-limit");
const connectDB = require("./Database/mongoose");

const app = express();
const cors_whitelist = [
    "http://localhost:3000",
    "https://cloud-shipping.klylylydeee.xyz"
];

const limiter = rateLimit({
    windowMs: 1 * 60 * 1000,
    max: 100,
    statusCode: 429,
    message: {
        status: 429,
        type: "error",
        message: "Max request to the server has been met. IP is temporarily banned from requesting."
    }
});
connectDB();

app.enable("trust proxy");

app.use(cors({
    origin: (origin, callback) => {
       cors_whitelist.indexOf(origin) !== -1 || !origin ?
            callback(null, true) :
            callback(new Error("Site Origin is not allowed by CORS"))
    },
    methods: "GET,PUT,PATCH,POST",
    preflightContinue: false,
    optionsSuccessStatus: 204,
}));
app.use(limiter);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(morgan(':date[web] :remote-addr :method :url :status :res[content-length] - :response-time ms'));
app.use(helmet());

app.listen(process.env.PORT || 5000, () =>
    console.log(`Running at port http://localhost:${process.env.PORT || 5000}/`)
);

app.get("/", async (req, res, next) => {
    res.status(200).send({
        "Project Name": "Cloud Shipping",
        "Project Overview": "Cloud Shipping provides services that fulfills individuals, business owners, and large organizations shipping transaction in a delightful experience through managing transactions and automate all your shipping needs.",
        "FrontEnd Repository": "https://github.com/Klylylydeee/tps-cloud-shipping-fe",
        "BackEnd Repository": "https://github.com/Klylylydeee/tps_cloud_shipping_be",
        "Contributors": [
            "Mary Valen",
            "Kenneth Artiaga Cañedo",
            "Guevarra Klyde"
        ]
    });
});

app.use("/authentication", require("./Routes/user.authentication"));
app.use("/transaction/user", require("./Routes/user.transactions"));
app.use("/transaction/partner", require("./Routes/delivery.transaction"));
app.use("/integration", require("./Routes/external.integration"));
app.use("/analytics", require("./Routes/external.analytics"));
app.use("/pages", require("./Routes/pages.transaction"));

app.use(require("./Controller/helper/error_handler").notFound);
app.use(require("./Controller/helper/error_handler").errorHandler);