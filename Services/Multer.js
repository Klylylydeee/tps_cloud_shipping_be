const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, callBack) => {
      callBack(null, 'Asset/MulterStorage')
  },
  filename: (req, file, callBack) => {
      callBack(null, `${file.originalname}`)
  }
})

module.exports = storage;