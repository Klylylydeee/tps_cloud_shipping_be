const puppeteer = require('puppeteer');

const generatePDF = async (html = "") => {
    const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
    const page = await browser.newPage();
    await page.emulateMediaType('print');
  
    await page.setContent(html);
    const buffer = await page.pdf({
        displayHeaderFooter: true,
        headerTemplate: `
        <div style="color: lightgray; border-top: solid lightgray 1px; font-size: 10px; padding-top: 5px; text-align: center; width: 100%;">
          <h1>This is a test message</h1> - <h1 class="pageNumber"></h1>
        </div>`,
        footerTemplate: `
          <div style="color: lightgray; border-top: solid lightgray 1px; font-size: 10px; padding-top: 5px; text-align: center; width: 100%;">
            <span>This is a test message</span> - <span class="pageNumber"></span>
          </div>
        `,
        format: 'A4',
        margin: {
          bottom: 70, // minimum required for footer msg to display
          left: 25,
          right: 35,
          top: 30
        },
        printBackground: true
    });

    await page.close();
    await browser.close();
  
    // return pdfBuffer;
    return buffer;
};  

const generateAssessment = async (html = "") => {
    const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox', '--allow-file-access-from-files', '--enable-local-file-accesses'] });
    const page = await browser.newPage();
    await page.emulateMediaType('print');
  
    await page.setContent(html);
    const buffer = await page.pdf({
        format: 'A3',
        headless: false,
        margin: {
          bottom: 70, // minimum required for footer msg to display
          left: 25,
          right: 35,
          top: 30
        },
        printBackground: true
    });

    await page.close();
    await browser.close();
  
    // return pdfBuffer;
    return buffer;
};  

module.exports = { generatePDF, generateAssessment };