const mailer = require('./Transport');
const handlebars = require('handlebars');
const path = require("path");
global.appRoot = path.resolve(`../${__dirname}`);
const fs = require('fs');

exports.sendRegistrationDataToClient = async (props) => {
    try {
        const content = fs.readFileSync('Services/Email/Template/SignUpClient.html', 'utf8');
        let template = handlebars.compile(content);
        let replacements = {
            first_name: props.firstname,
            last_name: props.last_name,
            username: props.username,
            email: props.email,
            phone_number: props.phone_number,
            password: props.password
        };
        let htmlToSend = template(replacements);
        const transporter = await mailer.transport();
        let data = await transporter.sendMail({
            to: props.email,
            cc: ``,
            bcc: ``,
            subject: `Welcome to Cloud Shipping, Client ${props.firstname} ${props.last_name}`,
            html: htmlToSend,
        //     attachments: [{
        //         filename: 'temp.jpg',
        //         path: __dirname +'/Template/Asset/temp.jpg',
        //         cid: 'logo' //my mistake was putting "cid:logo@cid" here! 
        //    }]
        //  <img src="cid:logo" alt="" />
        });
        return data
    } catch (err) {
        return err
    }
}

exports.sendRegistrationDataToDeliveryPartner = async (props) => {
    try {
        const content = fs.readFileSync('Services/Email/Template/SignUpDeliveryPartner.html', 'utf8');
        let template = handlebars.compile(content);
        let replacements = {
            first_name: props.firstname,
            last_name: props.last_name,
            username: props.username,
            email: props.email,
            phone_number: props.phone_number,
            password: props.password
        };
        let htmlToSend = template(replacements);
        const transporter = await mailer.transport();
        let data = await transporter.sendMail({
            to: props.email,
            cc: ``,
            bcc: ``,
            subject: `Welcome to Cloud Shipping , Delivery Partner ${props.firstname} ${props.last_name}`,
            html: htmlToSend
        });
        return data
    } catch (err) {
        return err
    }
}

exports.sendRegistrationDataToAPIIntegration = async (props) => {
    try {
        const content = fs.readFileSync('Services/Email/Template/SignUpIntegration.html', 'utf8');
        let template = handlebars.compile(content);
        let replacements = {
            username: props.username,
            organization_name: props.organization_name,
            email: props.email,
            phone_number: props.phone_number,
            industry: props.industry,
            token: props.token
        };
        let htmlToSend = template(replacements);
        const transporter = await mailer.transport();
        let data = await transporter.sendMail({
            to: props.email,
            cc: ``,
            bcc: ``,
            subject: `Welcome to Cloud Shipping , API Integration ${props.organization_name}`,
            html: htmlToSend
        });
        console.log(data)
        return data
    } catch (err) {
        return err
    }
}