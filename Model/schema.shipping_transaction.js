const { Schema, model } = require("mongoose");
const mongoose = require("mongoose");
const validator = require('validator');
const moment = require('moment-timezone');
const mongooseFuzzySearching = require('mongoose-fuzzy-searching');
const AutoIncrement = require('mongoose-sequence')(mongoose);
var Float = require('mongoose-float').loadType(mongoose);
const momentRandom = require('moment-random');
const { getLeocationValue } = require("../Controller/helper/geolocation");

const shippingSchema = new Schema({
    _id: {
        type: Number
    },
    transaction_number: {
        type: String
    },
    user_information: {
        type: {
            user: {
                type: String
            },
            organization: {
                type: String
            },
            user_role: {
                type: String,
                required: true
            },
            user_id: {
                type: String,
                required: true
            }
        },
        required: true
    },
    receiver_information: {
        type: {
            receiver_name: {
                type: String,
                required: true
            },
            receiver_phone_number: {
                type: Number,
                minLength: 11,
                maxLength: 11,
                required: true
            },
            receiver_email: {
                type: String,
                lowercase: true,
                required: true,
                trim: true,
                validate:{
                    validator: validator.isEmail,
                    message: 'Email is not a valid email',
                    isAsync: false
                }
            }
        },
        required: true
    },
    delivery_information: {
        type: {
            delivery_partner: {
                type: String
            },
            delivery_partner_id: {
                type: String
            },
            delivery_partner_picture: {
                type: String
            }
        },
        default: undefined
    },
    category: {
        type: String, 
        required: true,
        enum: [
            "Document Delivery",
            "Product Delivery",
            "Food Delivery",
        ]
    },
    pick_up_address: {
        type: {
            region: {
                type: String,
                enum: [
                    "REGION X",
                    "REGION XI",
                    "REGION XII",
                    "REGION XIII",
                    "REGION I",
                    "REGION II",
                    "REGION III",
                    "REGION IV-A",
                    "REGION IV-B",
                    "REGION V",
                    "REGION VI",
                    "REGION VII",
                    "REGION VIII",
                    "REGION IX",
                    "BARMM",
                    "CAR",
                    "NCR"
                ],
            },
            province: {
                type: String,
            },
            municipality: {
                type: String,
            },
            barangay: {
                type: String,
            },
            latitude: {
                type: String
            },
            longitude: {
                type: String
            }
        },
        required: true,
    },
    target_address: {
        type: {
            region: {
                type: String,
                enum: [
                    "REGION X",
                    "REGION XI",
                    "REGION XII",
                    "REGION XIII",
                    "REGION I",
                    "REGION II",
                    "REGION III",
                    "REGION IV-A",
                    "REGION IV-B",
                    "REGION V",
                    "REGION VI",
                    "REGION VII",
                    "REGION VIII",
                    "REGION IX",
                    "BARMM",
                    "CAR",
                    "NCR"
                ],
            },
            province: {
                type: String
            },
            municipality: {
                type: String
            },
            barangay: {
                type: String
            },
            latitude: {
                type: String
            },
            longitude: {
                type: String
            }
        },
        required: true,
    },
    status: {
        type: {
            sameRegion: {
                type: Boolean,
                required: true
            },
            updateText: {
                type: String,
                enum: [
                    "Shipping request is enqueued!",                                                                    // 1
                    "Shipping request has been accepted by the Delivery Partner and will be picking up the parcel!",    // 2
                    "Parcel has been dropped off to your Regional Sorting Center by the Delivery Partner!",             // 3
                    "Parcel is in transit to the destination Regional Sorting Center!",                                 // 4
                    "Parcel has been dropped off to the desitination Regional Sorting Center!",                         // 5
                    "Shipping request is now out for delivery! Please provide the QR to the Receiver!",                 // 6
                    "Shipping request is cancelled!",                                                                   // 7
                    "Shipping Transaction is Completed!"                                                                // 8
                ],
                default: "Shipping request is enqueued!",
                required: true
            },
            numStatus: {
                type: String,
                required: true,
                default: 1
            }
        },
        required: true
    },
    products: {
        type: [
            {
                product_name: {
                    type: String,
                    required: true
                },
                product_weight: {
                    type: String,
                    required: true
                }
            }
        ],
        required: true
    },
    shippingCost: {
        type: Float,
        required: true
    },
    notes: {
        type: String,
    }
},
{
    timestamps: { 
        currentTime: () => {
            // Random Date Generator
            return moment(momentRandom("2021-12-31", "2010-01-01")).utc("Asia/Singapore").format();
            // return moment().utc("Asia/Singapore").format();
        }
    }

});

shippingSchema.plugin(AutoIncrement, { inc_field: '_id' });

shippingSchema.post(
    'save',
    async function(doc) {
        doc.transaction_number = `TS${this._id}`
        let targetData = getLeocationValue(doc.target_address.province);
        let pickData = getLeocationValue(doc.pick_up_address.province);
        doc.target_address.latitude = targetData.langtitude;
        doc.target_address.longitude = targetData.longtitude;
        doc.pick_up_address.latitude = pickData.langtitude;
        doc.pick_up_address.longitude = pickData.longtitude;
        doc.save();
    }
);

shippingSchema.plugin(mongooseFuzzySearching, { fields: ["user_information.user", "user_information.delivery_partner"] })


const Shipping = model('shipping-transaction', shippingSchema);

module.exports = Shipping;