const { Schema, model } = require("mongoose");
const bcrypt = require("bcrypt");
const validator = require('validator');
const moment = require('moment-timezone');
const mongooseFuzzySearching = require('mongoose-fuzzy-searching');

const userSchema = new Schema({
    first_name: {
        type: String,
        lowercase: true,
    },
    last_name: {
        type: String,
        lowercase: true,
    },
    username: {
        type: String,
        lowercase: true,
        unique: true,
        trim: true
    },
    organization_name: {
        type: String,
        minLength: 3,
        default: undefined
    },
    user_picture: {
        type: String
    },
    organization_picture: {
        type: String
    },
    cloudinary_public_ip: {
        type: String,
        required: true
    },
    address: {
        type: {
            region: {
                type: String,
                enum: [
                    "REGION X",
                    "REGION XI",
                    "REGION XII",
                    "REGION XIII",
                    "REGION I",
                    "REGION II",
                    "REGION III",
                    "REGION IV-A",
                    "REGION IV-B",
                    "REGION V",
                    "REGION VI",
                    "REGION VII",
                    "REGION VIII",
                    "REGION IX",
                    "BARMM",
                    "CAR",
                    "NCR"
                ],
            },
            province: {
                type: String,
            },
            municipality: {
                type: String,
            },
            barangay: {
                type: String,
            }
        },
        required: true,
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true,
        trim: true,
        validate:{
            validator: validator.isEmail,
            message: 'Email is not a valid email',
            isAsync: false
        }
    },
    phone_number: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        maxLength: 11,
    },
    role: {
        type: String,
        enum: [
            "Client",
            "Delivery Partner",
            "API Integration Partner"
        ],
        required: true
    },
    industry: {
        type: String,
        enum: [
            "Farming",
            "Manufacturing",
            "Utilities",
            "Construction",
            "Retail",
            "Financial",
            "Communication",
            "Hospitality",
            "Information Technology",
            "Education",
            "Public Sector",
            "Research Development",
            "NA",
        ]
    },
    vehicles: {
        type: [
            {
                vehicle_type: {
                    type: String,
                },
                plate_no: {
                    type: String
                },
                vehicle_model: {
                    type: String
                },
                vehicle_picture: {
                    type: String
                },
                vehicle_cloudinary_public_ip: {
                    type: String
                }
            }
        ],
        default: undefined
    },
    licensed_no: {
        type: String
    },
    status: {
        type: Boolean,
        default: true
    },
    password: {
        type: String,
        minLength: 8,
        trim: true
    }
},
{
    timestamps: { 
        currentTime: () => {
            return moment().utc("Asia/Singapore").format();
        }
    }

});

userSchema.pre(
    'save',
    async function(next) {
        if(this.password != undefined){
            const hash = await bcrypt.hash(this.password, Number(process.env.HASHING));
            this.password = hash;
        }
        next();
    }
); 

userSchema.methods.isValidPassword = async function(password, userPassword) {
    const compare = await bcrypt.compare(password, userPassword);  
    return compare;
};

userSchema.plugin(mongooseFuzzySearching, { fields: ["username", "first_name", "last_name", "role", "organization_name", "industry"] })

const User = model('users', userSchema);

module.exports = User;